

// Custom JS 

/*
* @author: Faisal Saeed
*/


$(function SieveOfEratosthenesCached(n, cache) {
  $('#beginner-level-carousel').owlCarousel({
    loop: false,
    margin: 20,
    stagePadding: 0,
    autoplay: false,
    nav: false,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      },
      1645: {
        items: 6
      }
    }
  })


  $('#intermediate-level-carousel').owlCarousel({
    loop: false,
    margin: 20,
    stagePadding: 0,
    autoplay: false,
    nav: false,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      },
      1645: {
        items: 6
      }

    }
  })

  $('#expert-level-carousel').owlCarousel({
    loop: false,
    margin: 20,
    stagePadding: 0,
    autoplay: false,
    nav: false,
    dots: false,
    rtl: false,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
        margin: 30
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      },
      1645: {
        items: 6
      }

    }
  })

  

});


$(document).ready(function () {
  /*nice select initializer*/
  $(function () {
    $('select').niceSelect();
  });

  $('.dropdown').on('show.bs.dropdown', function () {
    $(this).find('.dropdown-menu').slideDown();
  });
  // Add slideUp animation to Bootstrap dropdown when collapsing.
  $('.dropdown').on('hide.bs.dropdown', function () {
    $(this).find('.dropdown-menu').slideUp();
  });

  /*add class in top logn form*/
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 500) {
      $("header").addClass("header-scroll ");
    } else {
      $("header").removeClass("header-scroll ");
    }
  });


  $('.viewmore-button').click(function () {
    $('.viewmore-items').slideToggle();
    if ($('.viewmore-button').text() == "View more") {
      $(this).text("View less")
    } else {
      $(this).text("View more")
    }
  });


  $(window).on('load', function () {
    $('#popup-modal').modal('show');
  });


  /* avatar uploader */
  var readURL = function (input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.profile-pic').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $(".file-upload").on('change', function () {
    readURL(this);
  });

  $(".upload-button").on('click', function () {
    $(".file-upload").click();
  });


});


/*search animation*/
function searchToggle(obj, evt) {
  var container = $(obj).closest('.search-wrapper');
  if (!container.hasClass('active')) {
    container.addClass('active');
    evt.preventDefault();
  }
  else if (container.hasClass('active') && $(obj).closest('.input-holder').length == 0) {
    container.removeClass('active');
    // clear input
    container.find('.search-input').val('');
  }
}


